# Effects of Coulomb Repulsion on the Phase Diagram of the Asakura-Oosawa Model #

Elizabeth Pemberton, Jason Haaga and Jim Gunton
Department of Physics, Lehigh University, Bethlehem, PA 18015

### Abstract: ###

The Asakura-Oosawa (AO) model describes the effective attractive interaction induced when
two large colloidal bodies are immersed in a solvent consisting of smaller macromolecules.
The effective interaction is induced due to an unbalanced osmotic pressure arising from the
depletion of the macromolecules in the region between the two bodies. We model
amelogenin, a hydrophobic protein and with a charged hydrophilic tail, and the polyether
compound polyethylene glycol (PEG). Amelogenin molecules are well described by a simplified
model consisting of a colloidal spherical monomer and tethered point charge. The point
charges interact through a screened Coulomb repulsion. Moreover, PEG-colloid interactions
are modeled by the AO interaction, which describes the depletion interaction between
amelogenin particles due to PEG. Since many particles in nature are charged, e.g. amelogenin,
we study the effects of Coulomb repulsion on the phase diagram of the AO model. To observe
this effect, we vary the magnitude of the point charges and compare the phase diagrams
generated from simulations.

This work has been supported by the National Science Foundation grant PHY-1359195.

## Paper on this research: Phase diagram of a model of the protein amelogenin published in Aug 2016 in the Journal of Chemical Physics ##